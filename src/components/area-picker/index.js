import React from 'react';
import { ButtonGroup, Button } from '@material-ui/core';
import { Room, LinearScale } from '@material-ui/icons';
import './styles.css';

export default function AreaPicker(props) {
  const { drawTypes, onDrawTypeSelected, drawType } = props;
  return (
    <div className="area-picker-container">
      <ButtonGroup color="primary" aria-label="outlined primary button group">
        {drawTypes.map(type => <Button variant={type.label === drawType ? "contained" : "outlined"} key={type.label} onClick={(event) => onDrawTypeSelected(type.label, event)} startIcon={type.icon}>{type.label}</Button>)}
      </ButtonGroup>
    </div>
  );
}
