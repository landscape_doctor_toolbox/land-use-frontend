import React, { useState } from 'react';
import {
  Paper, AppBar, Toolbar,
  Typography, makeStyles, ListItemText,
  List, ListItem, ListItemIcon, Collapse,
  ListItemSecondaryAction, Switch, IconButton,
  Tooltip, Menu, MenuItem
} from '@material-ui/core';
import { Info, MoreVert, LayersRounded, ExpandLess, ExpandMore, CloudDownload } from '@material-ui/icons';
import Proptypes from 'prop-types';

const useStyle = makeStyles((theme) => ({
  title: {
    textAlign: 'center',
    fontSize: '1.1em',
    border: '1px solid #e3e3e3',
    paddingBottom: '1em',
    paddingTop: '1em',
  },
  header: {
    display: "flex",
    justifyContent: "center",
    backgroundColor: '#00953b'
  },
  nested: {
    paddingLeft: theme.spacing(4),
  },
  row: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: "flex-end"
  }
}));

function Sidebar(props) {
  const [anchorEl, setAnchorEl] = React.useState(null);
  const [isForestMonitoringCollapsed, setIsForestMonitoringCollapsed] = useState(false);
  const open = Boolean(anchorEl);

  const { onSelectLayer, selectedLayers, spatialLayers } = props;

  const handleMenuVertClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleMenuVertClose = (downloadLink) => {
    setAnchorEl(null);
    const win = window.open(downloadLink, '_blank');
    if (win != null) {
      win.focus();
    }

  };

  const classes = useStyle();

  const menuOptions = ['Download'];

  const getDownloadButton = (downloadLink) => (
    <div>
      <IconButton
        aria-label="more"
        aria-controls="long-menu"
        aria-haspopup="true"
        onClick={handleMenuVertClick}
      >
        <MoreVert />
      </IconButton>
      <Menu
        id="long-menu"
        anchorEl={anchorEl}
        keepMounted
        open={open}
        onClose={handleMenuVertClose}
      >
        {menuOptions.map((option) => (
          <MenuItem key={option} onClick={() => alert(downloadLink)}>
            {option}
          </MenuItem>
        ))}
      </Menu>
    </div>
  );

  return (
    <Paper elevation={2} style={{ height: '100%' }}>
      <Typography variant="h6" className={classes.title}>
        Products
          </Typography>
      <List component="nav" aria-label="main mailbox folders">
        <ListItem
          button
          selected={selectedLayers.indexOf(spatialLayers[0].name) !== -1}
          onClick={() => onSelectLayer(spatialLayers[0].name, spatialLayers[0].url, spatialLayers[0].params)}>
          <ListItemIcon>
            <Switch size="small" checked={selectedLayers.indexOf(spatialLayers[0].name) !== -1}
              onChange={() => onSelectLayer(spatialLayers[0].name, spatialLayers[0].url, spatialLayers[0].params)}
            />
          </ListItemIcon>
          <ListItemText primary={spatialLayers[0].name} />
          <ListItemSecondaryAction >
            <div className={classes.row}>
              <Tooltip title={spatialLayers[0].description} placement="top">
                <IconButton size="small">
                  <Info fontSize="small" color="disabled" />
                </IconButton>
              </Tooltip>
              {getDownloadButton(spatialLayers[0].downloadLink)}
            </div>
          </ListItemSecondaryAction>
        </ListItem>
        <ListItem button
          onClick={() => setIsForestMonitoringCollapsed(!isForestMonitoringCollapsed)}>
          <ListItemIcon>
            <LayersRounded />
          </ListItemIcon>
          <ListItemText primary={spatialLayers[1].name} />
          {isForestMonitoringCollapsed ? <ExpandLess /> : <ExpandMore />}
        </ListItem>
        <Collapse in={isForestMonitoringCollapsed} timeout="auto" unmountOnExit>
          <List component="div" disablePadding>
            {
              spatialLayers[1].layers.map(item => (
                <ListItem button className={classes.nested}
                  selected={selectedLayers.indexOf(item.name) !== -1}
                  onClick={() => onSelectLayer(item.name, item.url, item.params)} key={item.name}>
                  <ListItemIcon>
                    <Switch size="small"
                      checked={props.selectedLayers.indexOf(item.name) !== -1}
                      name={item.name}
                      onChange={() => onSelectLayer(item.name, item.url, item.params)}
                    />
                  </ListItemIcon>
                  <ListItemText primary={item.name} />
                  <ListItemSecondaryAction >
                    <div className={classes.row}>
                      <Tooltip title={item.description} placement="top">
                        <IconButton size="small">
                          <Info fontSize="small" color="disabled" />
                        </IconButton>
                      </Tooltip>
                      <div onClick={() => handleMenuVertClose(item.downloadLink)} style={{ cursor: 'pointer' }}><CloudDownload /> </div>
                    </div>
                  </ListItemSecondaryAction>
                </ListItem>))
            }
          </List>
        </Collapse>
      </List>
    </Paper>
  );
}

Sidebar.propTypes = {
  onSelectLayer: Proptypes.func
};

export default Sidebar;

