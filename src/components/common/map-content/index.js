import React, { useEffect } from 'react';
import { Map, View, Feature } from 'ol';
import { Point } from 'ol/geom';
import { Tile, VectorImage, Vector as VectorLayer } from 'ol/layer';
import { OSM, Vector, XYZ, TileJSON } from 'ol/source';
import { GeoJSON } from 'ol/format';
import { Style, Fill, Stroke, Circle, Icon } from 'ol/style';
import { defaults } from 'ol/control';

import LayerTypeSwicher from '../../LayerTypeSwicher/LayerTypeSwicher';


import './index.css';

export default function MapContent(props) {
  const { onSetMap, map } = props;
  const OSMHumanitarianLayer = new Tile({
    source: new OSM({
      url: 'http://a.tile.openstreetmap.fr/hot/{z}/{x}/{y}.png'
    }),
    visible: false,
    title: 'OSMHumaniterian'
  });


  const OSMStandardLayer = new Tile({
    source: new OSM({
      url: 'https://{a-c}.tile.openstreetmap.org/{z}/{x}/{y}.png'
    }),
    visible: false,
    title: 'OSMStandard'
  });

  const stamenLayer = new Tile({
    source: new OSM({
      url: 'http://tile.stamen.com/terrain/{z}/{x}/{y}.png',
      attributions:
        'Map tiles by <a href="http://stamen.com">Stamen Design</a>, under <a href="http://creativecommons.org/licenses/by/3.0">CC BY 3.0</a>. Data by <a href="http://openstreetmap.org">OpenStreetMap</a>, under <a href="http://www.openstreetmap.org/copyright">ODbL</a>.'
    }),
    visible: false,
    title: 'Stamen'
  });

  const googleSatellite = new Tile({
    source: new XYZ({
      url: 'http://mt0.google.com/vt/lyrs=s&hl=en&x={x}&y={y}&z={z}'
    }),
    visible: false,
    title: 'Google Satellite'
  });

  const googleHybrid = new Tile({
    source: new XYZ({
      url: 'http://mt0.google.com/vt/lyrs=y&hl=en&x={x}&y={y}&z={z}'
    }),
    visible: true,
    title: 'Google Hybrid'
  });

  const terrianTile3D = new Tile({
    source: new TileJSON({
      url:
        'https://api.maptiler.com/tiles/terrain-quantized-mesh/tiles.json?key=VC8iwHDoiRxwVexzfwP7',
      attributions:
        '<a href="https://www.maptiler.com/copyright/" target="_blank">&copy; MapTiler</a> <a href="https://www.openstreetmap.org/copyright" target="_blank">&copy; OpenStreetMap contributors</a>'
    }),
    visible: true,
    title: 'Terrian Tile 3D'
  });

  const baseLayers = ['OSMHumaniterian', 'Terrian Tile 3D', 'Google Hybrid', 'Google Satellite', 'Stamen', 'OSMStandard'];

  const handleMapTypeChange = layerType => {
    if (map) {
      map.getLayers().forEach(element => {
        const title = element.get('title');
        element.setVisible(title === layerType || !baseLayers.includes(title));
      });
    }
  };

  useEffect(() => {
    const tempMap = new Map({
      controls: defaults({
        attribution: false,
        zoom: false
      }),
      target: 'landscape-diagnosis-map',
      view: new View({
        center: [4316382.914813424, 1003859.19199121],
        zoom: 6,
        minZoom: 1,
        maxZoom: 400
      }),
      layers: [
        OSMHumanitarianLayer,
        OSMStandardLayer,
        stamenLayer,
        googleSatellite,
        googleHybrid,
        terrianTile3D
      ]
    });
    onSetMap(tempMap);
  }, []);

  return (
    <div id="landscape-diagnosis-map" >
      <div
        style={{
          position: 'absolute',
          top: 20,
          left: 10,
          zIndex: 30,
          background: '#5bb75b',
          padding: 10,
          borderRadius: 20
        }}
      >
        <LayerTypeSwicher onLayerTypeChange={handleMapTypeChange} />
      </div>
    </div>

  );
}
