export default {
  BASE_URL: 'https://api.landscapedoctor.icog-labs.com/',
  GET_DATA: 'get-data',
  RECOMMENDATION_DATA: 'get-recommendation-data',
  SDG_DATA: 'get-sdg-data',
  STATIC_PATH: 'assets/data/download/'
};
