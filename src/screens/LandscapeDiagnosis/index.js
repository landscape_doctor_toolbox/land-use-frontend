import React, { useState, useEffect, useRef } from 'react';
import { Grid, LinearProgress, makeStyles, Typography, Paper, Chip, Popover, Button, Select, MenuItem, InputLabel, FormControl } from '@material-ui/core';
import { Image, VectorImage, Vector as VectorLayer } from 'ol/layer';
import { Feature } from 'ol';
import { Point } from 'ol/geom';
import { Vector, ImageWMS } from 'ol/source';
import { Draw } from 'ol/interaction';
import * as olProj from 'ol/proj';
import { Link } from 'react-router-dom';
import { Circle as CircleStyle, Fill, Stroke, Style, Icon } from 'ol/style';

import { GeoJSON } from 'ol/format';
import { LinearScale, Room, CloudUpload, Map } from '@material-ui/icons';
import { zoomIn, zoomOut } from '../../utils/map';

import { MapContent } from '../../components/common';
import Sidebar from '../../components/landscape-diagnosis/side-bar';
import { fetchSdgData } from '../../Api/services';
import { AreaPicker } from '../../components';
import MapControls from '../../components/MapControls/MapControls';

const shp = require('shpjs');

const image = new CircleStyle({
  radius: 5,
  fill: null,
  stroke: new Stroke({ color: 'red', width: 1 }),
});

const useStyles = makeStyles((theme) => ({
  primaryColor: {
    background: "#00953b",
  },
  card: {
    padding: theme.spacing(3),
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center'
  },
  texWhite: {
    color: "#fff"
  },
  link: {
    color: '#fff',
    fontSize: theme.spacing(1.5),
    marginTop: theme.spacing(3),
    fontStyle: 'italic'
  },
  button: {
    margin: theme.spacing(1),
  },
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
}));

const DRAW_TYPES = [
  { label: 'Point', icon: <Room /> },
  { label: 'Polygon', icon: <LinearScale /> },
  { label: 'Upload', icon: <CloudUpload /> },
  { label: 'Adminstrative Area', icon: <Map /> }
];

export default function LandscapeDiagnosis() {
  const DownloadLinkBase = "https://geoserver.landscapedoctor.icog-labs.com/geoserver/landscape_doctor/csw?service=CSW&version=2.0.2&request=DirectDownload&resourceId=landscape_doctor:";

  const spatialLayers = [
    {
      url: 'https://geoserver.landscapedoctor.icog-labs.com/geoserver/landscape_doctor/wms',
      params: { 'LAYERS': 'SDG15.3.1_indicator.tif:SDG15.3.1_indicator.tif' },
      serverType: 'geoserver',
      name: 'LDN',
      description: 'Description about LDN layer.',
      downloadLink: `${DownloadLinkBase}SDG15.3.1_indicator.tif`
    },
    {
      name: 'Forest Monitoring (Terra-I)',
      layers: [
        {
          url: 'https://geoserver.landscapedoctor.icog-labs.com/geoserver/landscape_doctor/wms',
          params: { 'LAYERS': 'Jan01_2004-Sep13_2020-increase:Jan01_2004-Sep13_2020-increase' },
          serverType: 'geoserver',
          name: 'Forestation',
          description: 'Description about forestation forest monitoring layer.',
          downloadLink: `${DownloadLinkBase}Jan01_2004-Sep13_2020-increase`
        },
        {
          url: 'https://geoserver.landscapedoctor.icog-labs.com/geoserver/landscape_doctor/wms',
          params: { 'LAYERS': 'Jan01_2004-Sep13_2020-decrease:Jan01_2004-Sep13_2020-decrease' },
          serverType: 'geoserver',
          name: 'Deforestation',
          description: 'Description about deforestation forest monitoring layer.',
          downloadLink: `${DownloadLinkBase}Jan01_2004-Sep13_2020-decrease`
        }, {
          url: 'https://geoserver.landscapedoctor.icog-labs.com/geoserver/landscape_doctor/wms',
          params: { 'LAYERS': 'Jan01_2004-Sep13_2020-floods:Jan01_2004-Sep13_2020-floods' },
          serverType: 'geoserver',
          name: 'Flood',
          description: 'Description about flood forest monitoring layer.',
          downloadLink: `${DownloadLinkBase}Jan01_2004-Sep13_2020-floods`
        }]
    },
  ];

  const [selectedLayers, setSelectedLayers] = useState([]);

  const SdgValues = ['degraded', 'improvement', 'stable'];
  const classes = useStyles();
  const [map, setMap] = useState(null);
  const [selectedLocation, setSelectedLocation] = useState({});
  const [loading, setLoading] = useState(false);
  const [drawType, setDrawType] = useState(DRAW_TYPES[0].label);
  const [draw, setDraw] = useState(null);
  const [anchorEl, setAnchorEl] = React.useState(null);
  const [adminstrativeArea, setAdminstrativeArea] = useState(null);
  const [adminstrativeAreaGeojson, setAdminstrativeAreaGeojson] = useState(null);
  const [adminAreaSelections, setAdminAreaSelections] = useState({
    admin0: '',
    admin1: '',
    admin2: ''
  });
  const latestFeature = useRef(null);// store latest point feature on draw layer 

  const openPopover = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const open = Boolean(anchorEl);
  const id = open ? 'simple-popover' : undefined;
  const iconFeature = new Feature({
    geometry: new Point(olProj.fromLonLat(selectedLocation.coord ? [selectedLocation.coord.lon, selectedLocation.coord.lat] : [38.746912, 9.011146])),
    name: 'marker',
  });

  const getProjectedGeojson = (geojsonSource) => {
    // convert geojson 'EPSG:4326' projection to 'EPSG:3857'
    const geom = [];
    geojsonSource.forEachFeature((feature) => {
      const { geometry, ...otherProperties } = feature.getProperties();
      geom.push(new Feature({
        geometry: feature.getGeometry().clone().transform('EPSG:4326', 'EPSG:3857'),
        ...otherProperties
      }));
    });
    const writer = new GeoJSON();
    const geoJsonStr = writer.writeFeatures(geom);
    return geoJsonStr;
  };

  const vectorSource = new Vector({ wrapX: false });

  const polygonLayer = new VectorLayer({
    source: vectorSource,
    title: 'polygon'
  });

  const layer = new VectorLayer({
    source: new Vector({
      features: [iconFeature],
    }),
    title: 'marker',
    style: new Style({
      image: new Icon({
        anchor: [0.5, 46],
        anchorXUnits: 'fraction',
        anchorYUnits: 'pixels',
        src: 'https://img.icons8.com/ultraviolet/40/000000/marker.png'
      })
    })
  });

  const removeLayerByName = (mapRef, layerName) => {
    mapRef.getLayers().getArray()
      .filter(layerItem => layerItem.get('title') === layerName)
      .forEach(item => map.removeLayer(item));
  };

  const updateLegend = (resolution, source, name) => {
    const graphicUrl = source.getLegendUrl(resolution);
    const img = document.getElementById(name);
    if (img) {
      img.src = graphicUrl;
    } else {
      const legendsContainer = document.getElementById('legends');
      const legend = document.createElement('img');
      legend.src = graphicUrl;
      legend.id = name;
      legend.style.width = '80%';
      legend.style.marginRight = '1em';
      legendsContainer.appendChild(legend);
    }
  };

  const handleSelectLayer = (name, url, params) => {
    // check if the selected layer is already selected
    // if it is selected remove it
    if (selectedLayers.indexOf(name) !== -1 && map) {
      setSelectedLayers(selectedLayers.filter(item => item !== name));
      removeLayerByName(map, name); // remove layer
      const legend = document.getElementById(name);
      legend && legend.remove();
      return;
    }
    // if name not exist on selected list, add to the list
    setSelectedLayers(selectedLayers.concat(name));
    const layerSource = new ImageWMS({
      url,
      params: { ...params },
      serverType: 'geoserver'
    });
    const overlayLayer = new Image({
      source: layerSource,
      title: name
    });
    if (map) {
      // layer.setOpacity(0.5);
      map.addLayer(overlayLayer);
      const resolution = map.getView().getResolution();
      updateLegend(resolution, layerSource, name);
      // map.getView().on('change:resolution', function (event) {
      //   const mapResolution = event.target.getResolution();
      //   updateLegend(mapResolution, layerSource, name);
      // });
    }
  };

  const initializeLayer = () => {
    handleSelectLayer(spatialLayers[0].name, spatialLayers[0].url, spatialLayers[0].params);
  };

  const handleMapClicked = (event) => {
    const lonLat = olProj.toLonLat(event.coordinate);
    setLoading(true);
    fetchSdgData(lonLat[0], lonLat[1])
      .then(res => {
        if (res.result) {
          setSelectedLocation({ coord: { lon: lonLat[0], lat: lonLat[1] }, result: res.result });
        }
        setLoading(false);
      })
      .catch(_ => {
        setLoading(false);
      });
  };

  const fitMapToExtent = (map, extent) => {
    if (map) map.getView().fit(extent, map.getSize());
  };

  const handlePloygonDrawFinished = (event) => {
    if (drawType === 'Point') {
      try {
        if (latestFeature.current) vectorSource.removeFeature(latestFeature.current);
        latestFeature.current = event.feature;
      } catch (error) {
        console.log(error);
      }
    }
    const extent = event.feature.getGeometry().getExtent();
    if (drawType !== 'Point') {
      fitMapToExtent(map, extent);
    }
  };

  const addInteraction = (map) => {
    const tempDraw = new Draw({
      source: vectorSource,
      type: drawType
    });
    setDraw(tempDraw);
    tempDraw.on('drawstart', (e) => {
      if (drawType !== 'Point') {
        vectorSource.clear();  // clear previous features
      }
    });
    tempDraw.on('drawend', handlePloygonDrawFinished);
    map.removeInteraction(draw);
    map.addInteraction(tempDraw);
  };

  const renderLegend = () => {
    return (
      <div style={{ margin: "1em", position: 'absolute', bottom: 0, display: 'flex' }} id="legends" />);
  };

  const openFileChooser = () => {
    const fileInput = document.getElementById('shapeFileInput');
    if (fileInput) fileInput.click();
  };

  // Geojson style 
  const styles = {
    'Point': new Style({
      image,
    }),
    'LineString': new Style({
      stroke: new Stroke({
        color: 'green',
        width: 1,
      }),
    }),
    'MultiLineString': new Style({
      stroke: new Stroke({
        color: 'green',
        width: 1,
      }),
    }),
    'MultiPoint': new Style({
      image,
    }),
    'MultiPolygon': new Style({
      stroke: new Stroke({
        color: 'blue',
        width: 3,
      }),
      fill: new Fill({
        color: 'rgba(255, 255, 0, 0.1)',
      }),
    }),
    'Polygon': new Style({
      stroke: new Stroke({
        color: 'blue',
        lineDash: [0],
        width: 3,
      }),
      fill: new Fill({
        color: 'rgba(255, 255, 0, 0.1)',
      }),
    }),
    'GeometryCollection': new Style({
      stroke: new Stroke({
        color: 'magenta',
        width: 2,
      }),
      fill: new Fill({
        color: 'magenta',
      }),
      image: new CircleStyle({
        radius: 10,
        fill: null,
        stroke: new Stroke({
          color: 'magenta',
        }),
      }),
    }),
    'Circle': new Style({
      stroke: new Stroke({
        color: 'red',
        width: 2,
      }),
      fill: new Fill({
        color: 'rgba(255,0,0,0.2)',
      }),
    }),
  };

  const styleFunction = function (feature) {
    return styles[feature.getGeometry().getType()];
  };

  const styleAdminAreaLayer = (feature, layerTitle) => {
    const data = feature.getProperties();
    const areaCode0 = data.ADM0_CODE;
    const areaCode1 = data.ADM1_CODE;
    const areaCode2 = data.ADM2_CODE;
    console.log({ areaCode0, areaCode1, areaCode2 }, adminAreaSelections, '----');
    if (layerTitle === 'Admin0' && areaCode0 === adminAreaSelections.admin0 && adminAreaSelections.admin1 === '') {
      fitMapToExtent(map, feature.getGeometry().getExtent());
    }
    if (layerTitle === 'Admin1' && areaCode1 === adminAreaSelections.admin1 && adminAreaSelections.admin2 === '') {
      fitMapToExtent(map, feature.getGeometry().getExtent());
    }
    if (layerTitle === 'Admin2' && areaCode2 === adminAreaSelections.admin2) {
      fitMapToExtent(map, feature.getGeometry().getExtent());
    }
    if (layerTitle === 'Admin0') {
      return (areaCode0 === adminAreaSelections.admin0 && adminAreaSelections.admin1 === '') ? styles[feature.getGeometry().getType()] : null;
    }
    if (layerTitle === 'Admin1') {
      return (areaCode1 === adminAreaSelections.admin1 && adminAreaSelections.admin2 === '') ? styles[feature.getGeometry().getType()] : null;
    }
    if (layerTitle === 'Admin2') {
      return areaCode2 === adminAreaSelections.admin2 ? styles[feature.getGeometry().getType()] : null;
    }
    return null;
  };

  const renderShapeOnMap = async (event) => {
    const fileBuffer = await event.target.files[0]?.arrayBuffer();
    shp(fileBuffer).then(geojson => {
      const geojsonSource = new Vector({
        features: new GeoJSON().readFeatures(geojson[0] ? geojson[0] : geojson)
      });
      // convert geojson 'EPSG:4326' projection to 'EPSG:3857'
      const geom = [];
      geojsonSource.forEachFeature((feature) => {
        const { geometry, ...otherProperties } = feature.getProperties();
        geom.push(new Feature({
          geometry: feature.getGeometry().clone().transform('EPSG:4326', 'EPSG:3857'),
          ...otherProperties
        }));
      });
      const writer = new GeoJSON();
      const geoJsonStr = writer.writeFeatures(geom);
      const shapeSource = new Vector({
        features: new GeoJSON().readFeatures(geoJsonStr)
      });
      const shapeLayer = new VectorLayer({
        source: shapeSource,
        title: 'shape',
        style: styleFunction,
      });
      fitMapToExtent(map, shapeSource.getExtent());
      // remove privously added layer
      map.getLayers().forEach(item => {
        if (item && item.get('title') === 'shape') {
          map.removeLayer(item);
        }
      });

      // console.log(geojson, shapeLayer, '--===--===');
      map.addLayer(shapeLayer);
      // console.log(map.getLayers());
      setDrawType(DRAW_TYPES[0].label);
    })
      .catch(error => {
        console.log(error);
      });
  };


  /**
   * 
   * @param {Array} adminAreaGeojson [admin0Geojson, admin1Geojson, admin2Geojson]
   */
  const addAdminstrativeMapAreaLayer = (adminAreaGeojson, map) => {

    // remove previously added layer
    if (map) {
      map.getLayers().forEach(item => {
        if (item && (item.get('title') === 'Admin0' || item.get('title') === 'Admin1' || item.get('title') === 'Admin2')) {
          map.removeLayer(item);
        }
      });
    }

    // Admin level 0 layer
    const AdminAreaLayers = adminAreaGeojson.map((geojson, index) => {
      return new VectorLayer({
        source: new Vector({
          features: new GeoJSON().readFeatures(geojson)
        }),
        title: `Admin${index}`,
        style: feature => styleAdminAreaLayer(feature, `Admin${index}`),
      });
    });
    if (map) {
      AdminAreaLayers.forEach(layerItem => map.addLayer(layerItem));
    }
  };

  const renderPopover = () => {
    return <Popover
      id={id}
      open={open}
      anchorEl={anchorEl}
      onClose={handleClose}
      anchorOrigin={{
        vertical: 'top',
        horizontal: 'center',
      }}
      transformOrigin={{
        vertical: 'top',
        horizontal: 'center',
      }}
    >
      {drawType === DRAW_TYPES[2].label && <>
        <Button
          variant="contained"
          color="default"
          className={classes.button}
          startIcon={<CloudUpload />}
          onClick={openFileChooser}
        >
          Upload File
  </Button>
        <input id="shapeFileInput" type="file" style={{ visibility: 'hidden' }} onChange={renderShapeOnMap} accept=".zip,.rar,.7zip" />
      </>}

      {drawType === DRAW_TYPES[3].label && <div>
        <FormControl className={classes.formControl}>
          <p style={{ margin: 0, padding: 0, color: 'rgb(175 170 170)', fontSize: '.5em' }}>Admin 0</p>
          <Select
            labelId="demo-simple-select-label"
            id="demo-simple-select"
            placeholder='Admin 0'
            value={adminAreaSelections.admin0}
            onChange={event => {
              setAdminAreaSelections({ ...adminAreaSelections, admin0: event.target.value, admin1: '', admin2: '' });
            }}
          >
            {adminstrativeArea && adminstrativeArea[0]?.features.map(feature => <MenuItem value={feature.properties.ADM0_CODE}>{feature.properties.ADM0_NAME}</MenuItem>)}

          </Select>
          <p style={{ margin: 0, padding: 0, color: 'rgb(175 170 170)', fontSize: '.5em' }}>Admin 1</p>
          <Select
            labelId="demo-simple-select-label"
            id="demo-simple-select"
            placeholder="Admin 1"
            value={adminAreaSelections.admin1}
            onChange={event => {
              setAdminAreaSelections({ ...adminAreaSelections, admin1: event.target.value, admin2: '' });
            }}
          >
            {adminstrativeArea && adminstrativeArea[1]?.features.filter(feature => adminAreaSelections.admin0 !== '' ? feature.properties.ADM0_CODE === adminAreaSelections.admin0 : false)?.map(feature => <MenuItem value={feature.properties.ADM1_CODE}>{feature.properties.ADM1_NAME}</MenuItem>)}

          </Select>
          <p style={{ margin: 0, padding: 0, color: 'rgb(175 170 170)', fontSize: '.5em' }}>Admin 2</p>
          <Select
            labelId="demo-simple-select-label"
            id="demo-simple-select"
            placeholder="Admin 2"
            value={adminAreaSelections.admin2}
            onChange={event => {
              setAdminAreaSelections({ ...adminAreaSelections, admin2: event.target.value });
            }}
          >
            {adminstrativeArea && adminstrativeArea[2]?.features.filter(feature => adminAreaSelections.admin1 !== '' ? feature.properties.ADM1_CODE === adminAreaSelections.admin1 : false)?.map(feature => <MenuItem value={feature.properties.ADM2_CODE}>{feature.properties.ADM2_NAME}</MenuItem>)}

          </Select>
        </FormControl></div>}

    </Popover>;
  };

  const clearPreviouslyDrawnFeatures = () => {
    vectorSource.clear(); // clear draw interaction layer source
    latestFeature.current = null;
    // remove privously added  shape layer
    if (map) {
      map.getLayers().forEach(item => {
        if (item && item.get('title') === 'shape') {
          map.removeLayer(item);
        }
      });
    }
    // clear adminstrative area selections
    setAdminAreaSelections({
      admin0: "",
      admin1: "",
      admin2: ""
    });
  };

  const handleDrawTypeSelected = (type, event) => {
    clearPreviouslyDrawnFeatures();
    console.log(map.getLayers().array_);
    if (type === DRAW_TYPES[2].label || type === DRAW_TYPES[3].label) {
      // Todo: open popover
      openPopover(event);
      console.log(map.getLayers(),);
    }
    setDrawType(type);

  };

  const loadAdminstrativeLayers = (map) => {
    console.log('start loading adminstrative area');
    shp("assets/data/shapes.zip").then((geojson) => {
      // admin level 0
      const admin0GeojsonSource = new Vector({
        features: new GeoJSON().readFeatures(geojson[0])
      });
      const admin0Geojson = JSON.parse(getProjectedGeojson(admin0GeojsonSource));
      // admin level 1
      const admin1GeojsonSource = new Vector({
        features: new GeoJSON().readFeatures(geojson[1])
      });
      const admin1Geojson = JSON.parse(getProjectedGeojson(admin1GeojsonSource));
      // admin level 2
      const admin2GeojsonSource = new Vector({
        features: new GeoJSON().readFeatures(geojson[2])
      });
      const admin2Geojson = JSON.parse(getProjectedGeojson(admin2GeojsonSource));
      console.log([
        admin0Geojson, admin1Geojson, admin2Geojson
      ], 'Administrative layer');

      addAdminstrativeMapAreaLayer([admin0Geojson, admin1Geojson, admin2Geojson], map);
      setAdminstrativeAreaGeojson([admin0Geojson, admin1Geojson, admin2Geojson]);
      setAdminstrativeArea(geojson);
    })
      .catch(error => {
        console.log(error);
      });
  };

  useEffect(() => {
    if (map) {
      map.on('click', handleMapClicked);
      initializeLayer();
      loadAdminstrativeLayers(map);
    }
  }, [map]);

  useEffect(() => {
    if (map) {
      map.getLayers().forEach(item => {
        if (item && item.get('title') === 'marker') {
          map.removeLayer(item);
        }
      });
      map.addLayer(layer);
    }
  }, [map, selectedLocation]);

  useEffect(() => {
    if (map) {
      map.getLayers().forEach(item => {
        if (item && item.get('title') === 'polygon') {
          map.removeLayer(item);
        }
      });
      map.addLayer(polygonLayer);
      addInteraction(map);
    }
  }, [map, drawType]);

  useEffect(() => {
    adminstrativeAreaGeojson &&
      addAdminstrativeMapAreaLayer(adminstrativeAreaGeojson, map);
  }, [JSON.stringify(adminAreaSelections)]);

  const renderRestorationLink = (value) => {
    return value === SdgValues[0] &&
      <Link to={
        {
          pathname: '/landscape-restoration',
          state: {
            lat: selectedLocation.coord.lat,
            lon: selectedLocation.coord.lon,
          }
        }
      } className={classes.link}>
        Please find possible restoration methods
    </Link>;
  };

  const renderSdgInfo = () => {
    return (selectedLocation.coord && !loading ?
      <div style={{ position: 'absolute', bottom: 80, right: 20 }}>
        <Paper elevation={3} className={`${classes.card} ${classes.primaryColor}`} >
          {
            <Typography variant="h5" className={classes.texWhite} gutterBottom>
              {parseInt(selectedLocation.coord.lat).toFixed(2)}° N, {parseInt(selectedLocation.coord.lon).toFixed(2)}° E
        </Typography>

          }
          {selectedLocation.result ? <Chip
            label={selectedLocation.result}
          /> : null}
          {renderRestorationLink(selectedLocation.result)}
        </Paper>
      </div>
      : null);
  };

  return (
    <div className="diagnosis-container">
      <Grid container spacing={0}>
        <Grid item xs={3}>
          <div style={{ height: '100%', width: '100%' }} >
            <Sidebar onSelectLayer={handleSelectLayer}
              spatialLayers={spatialLayers}
              selectedLayers={selectedLayers} />
          </div>
        </Grid>
        <Grid item xs={9}>
          <div style={{ position: 'relative' }}>
            {loading && <LinearProgress className={classes.primaryColor} />}
            <MapContent map={map} onSetMap={setMap} selectedLocation={selectedLocation} />
            <MapControls
              onZoomIn={() => zoomIn(map)}
              onZoomOut={() => zoomOut(map)}
            />
            {renderSdgInfo()}
            {renderRestorationLink()}
            {renderLegend()}
            <div>
              <AreaPicker drawTypes={DRAW_TYPES} onDrawTypeSelected={handleDrawTypeSelected} drawType={drawType} />
              {renderPopover()}
            </div>
          </div>
        </Grid>
      </Grid>
    </div >
  );
}
