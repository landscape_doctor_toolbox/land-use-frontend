import React, { useState, useEffect } from 'react';
import { Grid, LinearProgress, makeStyles } from '@material-ui/core';
import { Image, Vector as VectorLayer } from 'ol/layer';
import { Style, Fill, Stroke, Circle } from 'ol/style';
import { Feature } from 'ol';
import { Point } from 'ol/geom';
import { Vector, ImageWMS } from 'ol/source';

import * as olProj from 'ol/proj';

import { zoomOut, zoomIn } from '../../utils/map';
import MapControls from '../../components/MapControls/MapControls';
import { Sidebar, MapContent } from '../../components/common';

const useStyles = makeStyles((theme) => ({
  primaryColor: {
    background: "#00953b",
  },
  card: {
    padding: theme.spacing(3),
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center'
  },
  texWhite: {
    color: "#fff"
  },
  link: {
    color: '#fff',
    fontSize: theme.spacing(1.5),
    marginTop: theme.spacing(3),
    fontStyle: 'italic'
  }
}));

export default function GeoSpatialLayers() {

  const DownloadLinkBase = "https://geoserver.landscapedoctor.icog-labs.com/geoserver/landscape_doctor/csw?service=CSW&version=2.0.2&request=DirectDownload&resourceId=landscape_doctor:";

  const spatialLayers = [
    {
      name: 'Agro-ecology',
      url: 'https://geoserver.landscapedoctor.icog-labs.com/geoserver/landscape_doctor/wms',
      params: { 'LAYERS': 'landuse:aez' },
      description: 'Description about agro ecology layer.',
      downloadLink: `${DownloadLinkBase}landuse:aez`
    },
    {
      name: 'Climatic Factors',
      layers: [
        {
          name: 'Precipitation',
          url: 'https://geoserver.landscapedoctor.icog-labs.com/geoserver/landscape_doctor/wms',
          params: {
            'LAYERS': 'Precipitation_Annual.tif:Precipitation_Annual.tif'
          },
          description: 'Description about agro precipitation layer.',
          downloadLink: `${DownloadLinkBase}Precipitation_Annual.tif`
        },

      ]
    },
    {
      name: 'Topographic',
      layers: [
        {
          name: "Elevation", url: 'https://geoserver.landscapedoctor.icog-labs.com/geoserver/landscape_doctor/wms',
          params: {
            'LAYERS': 'elevation.tif:elevation.tif',
          },
          description: 'Description about elevation layer.',
          downloadLink: `${DownloadLinkBase}elevation.tif`
        },
        {
          name: 'Profile Curvature',
          url: 'https://geoserver.landscapedoctor.icog-labs.com/geoserver/landscape_doctor/wms',
          params: { 'LAYERS': 'profile_curvature.tif:profile_curvature.tif' },
          description: 'Description about profile curveture layer.',
          downloadLink: `${DownloadLinkBase}profile_curvature.tif`
        },
        {
          name: 'Land Form',
          url: 'https://geoserver.landscapedoctor.icog-labs.com/geoserver/landscape_doctor/wms',
          params: { 'LAYERS': 'land_form.tif:land_form.tif' },
          description: 'Description about land form layer.',
          downloadLink: `${DownloadLinkBase}land_form.tif`
        },
        {
          name: 'Topographic Slope',
          url: 'https://geoserver.landscapedoctor.icog-labs.com/geoserver/landscape_doctor/wms',
          params: { 'LAYERS': 'topographic_slope_new.tif:topographic_slope_new.tif' },
          description: 'Description about topographic slope layer.',
          downloadLink: `${DownloadLinkBase}topographic_slope_new.tif`
        },
      ]
    },
    {
      name: 'Soil Characteristics',
      layers: [
        {
          name: 'Clay Composition',
          url: 'https://geoserver.landscapedoctor.icog-labs.com/geoserver/landscape_doctor/wms',
          params: { 'LAYERS': 'Soil_Clay_April2004.tif:Soil_Clay_April2004.tif' },
          description: 'Description about soil clay layer.',
          downloadLink: `${DownloadLinkBase}Soil_Clay_April2004.tif`
        },
        {
          name: 'Sand Composition',
          url: 'https://geoserver.landscapedoctor.icog-labs.com/geoserver/landscape_doctor/wms',
          params: { 'LAYERS': 'Soil_Sand.tif:Soil_Sand.tif' },
          description: 'Description about sand composition layer.',
          downloadLink: `${DownloadLinkBase}Soil_Sand.tif`
        },
        {
          name: 'Silt composition',
          url: 'https://geoserver.landscapedoctor.icog-labs.com/geoserver/landscape_doctor/wms',
          params: { 'LAYERS': 'Soil_Silt.tif:Soil_Silt.tif' },
          description: 'Description about sand silt composition.',
          downloadLink: `${DownloadLinkBase}Soil_Silt.tif`
        },
        {
          name: 'Soil Organic Carbon',
          url: 'https://geoserver.landscapedoctor.icog-labs.com/geoserver/landscape_doctor/wms',
          params: { 'LAYERS': 'Soil_Organic_Carbon.tif:Soil_Organic_Carbon.tif' },
          description: 'Description about Soil Organic Carbon.',
          downloadLink: `${DownloadLinkBase}Soil_Organic_Carbon.tif`
        },
      ]
    }
  ];

  const classes = useStyles();
  const [map, setMap] = useState(null);
  const [selectedLocation] = useState({});
  const [loading] = useState(false);
  const [selectedLayers, setSelectedLayers] = useState([]);

  const layer = new VectorLayer({
    source: new Vector({
      features: [
        new Feature({
          type: 'marker',
          geometry: new Point(
            olProj.fromLonLat(selectedLocation || [38.746912, 9.011146])
          )
        }),
        new Feature({
          type: 'point',
          geometry: new Point(
            olProj.fromLonLat(selectedLocation || [38.746912, 9.011146])
          )
        })
      ]
    }),
    style: new Style({
      image: new Circle({
        radius: 7,
        fill: new Fill({ color: 'black' }),
        stroke: new Stroke({
          color: 'white',
          width: 2
        })
      })
    }),
    title: 'marker'
  });

  const updateLegend = (resolution, source, name) => {
    const graphicUrl = source.getLegendUrl(resolution);
    const img = document.getElementById(name);
    if (img) {
      img.src = graphicUrl;
    } else {
      const legendsContainer = document.getElementById('legends');
      const legend = document.createElement('img');
      legend.src = graphicUrl;
      legend.id = name;
      legend.style.width = '80%';
      legend.style.marginRight = '1em';
      legendsContainer.appendChild(legend);
    }
  };

  const removeLayerByName = (mapRef, layerName) => {
    mapRef.getLayers().getArray()
      .filter(layerItem => layerItem.get('title') === layerName)
      .forEach(item => map.removeLayer(item));
  };

  const handleSelectLayer = (name, url, params) => {
    // check if the selected layer is already selected
    // if it is selected remove it
    if (selectedLayers.indexOf(name) !== -1 && map) {
      setSelectedLayers(selectedLayers.filter(item => item !== name));
      removeLayerByName(map, name); // remove layer
      const legend = document.getElementById(name);
      legend && legend.remove();
      return;
    }
    // if name not exist on selected list, add to the list
    setSelectedLayers(selectedLayers.concat(name));
    const layerSource = new ImageWMS({
      url,
      params: { ...params },
      serverType: 'geoserver'
    });
    const overlayLayer = new Image({
      source: layerSource,
      title: name
    });
    if (map) {
      layer.setOpacity(0.5);
      map.addLayer(overlayLayer);
      const resolution = map.getView().getResolution();
      updateLegend(resolution, layerSource, name);
      // map.getView().on('change:resolution', function (event) {
      //   const mapResolution = event.target.getResolution();
      //   updateLegend(mapResolution, layerSource);
      // });
    }
  };

  useEffect(() => {
    handleSelectLayer(spatialLayers[0].name, spatialLayers[0].url, spatialLayers[0].params);
  }, []);

  useEffect(() => {
    if (map) {
      map.getLayers().forEach(item => {
        if (item && item.get('title') === 'marker') {
          map.removeLayer(item);
        }
      });
      map.addLayer(layer);
    }
  }, [map, selectedLocation]);

  const renderLegend = () => {
    return (
      <div style={{ margin: "1em", position: 'absolute', bottom: 0, display: 'flex' }} id="legends" />);
  };

  return (
    <div className="diagnosis-container">
      <Grid container spacing={0}>
        <Grid item xs={3}>

          <div style={{ height: '100%', width: '100%' }} >
            <Sidebar onSelectLayer={handleSelectLayer}
              spatialLayers={spatialLayers}
              selectedLayers={selectedLayers} />

          </div>
        </Grid>
        <Grid item xs={9}>
          <div style={{ position: 'relative' }}>
            <MapContent map={map} onSetMap={setMap} selectedLocation={selectedLocation} />
            <MapControls
              onZoomIn={() => zoomIn(map)}
              onZoomOut={() => zoomOut(map)}
            />
            {renderLegend()}
          </div>
        </Grid>
      </Grid>
    </div>
  );
}
